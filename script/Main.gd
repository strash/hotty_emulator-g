extends Node


enum { MENU, LEVELS, GAME }


# BUILTINS - - - - - - - - -


func _ready() -> void:
	var _btn_start_pressed = $Menu.connect("btn_start_pressed", self, "_on_Menu_btn_start_pressed")
	var _btn_levels_pressed = $Menu.connect("btn_levels_pressed", self, "_on_Menu_btn_levels_pressed")

	var _btn_back_levels_pressed = $Levels.connect("btn_back_pressed", self, "_on_Levels_btn_back_pressed")
	var _btn_lvl_pressed = $Levels.connect("btn_lvl_pressed", self, "_on_Levels_btn_lvl_pressed")

	var _btn_back_game_pressed = $Game.connect("btn_back_pressed", self, "_on_Game_btn_back_pressed")

	show_view(-1)


func show_view(view: int) -> void:
	match view:
		MENU:
			$Menu.show()
			$Tween.interpolate_property($Menu, "modulate:a", 0.0, 1.0, 0.3)
			$Tween.interpolate_property($Levels, "modulate:a", 1.0, 0.0, 0.3)
			if not $Tween.is_active():
				$Tween.start()
			yield(get_tree().create_timer(0.3), "timeout")
			$Levels.hide()
			$Game.hide()
		LEVELS:
			$Levels.show()
			$Tween.interpolate_property($Menu, "modulate:a", 1.0, 0.0, 0.3)
			$Tween.interpolate_property($Levels, "modulate:a", 0.0, 1.0, 0.3)
			if not $Tween.is_active():
				$Tween.start()
			yield(get_tree().create_timer(0.3), "timeout")
			$Menu.hide()
			$Game.hide()
		GAME:
			$Game.show()
			$Tween.interpolate_property($Menu, "modulate:a", 1.0, 0.0, 0.3)
			$Tween.interpolate_property($Levels, "modulate:a", 1.0, 0.0, 0.3)
			$Tween.interpolate_property($Game, "modulate:a", 0.0, 1.0, 0.3)
			if not $Tween.is_active():
				$Tween.start()
			yield(get_tree().create_timer(0.3), "timeout")
			$Menu.hide()
			$Levels.hide()
		_:
			$Menu.show()
			$Levels.hide()
			$Game.hide()


# METHODS - - - - - - - - -


# SIGNALS - - - - - - - - -


func _on_Menu_btn_start_pressed() -> void:
	$Game.load_level(1)
	show_view(GAME)


func _on_Menu_btn_levels_pressed() -> void:
	show_view(LEVELS)


func _on_Levels_btn_back_pressed() -> void:
	show_view(MENU)


func _on_Levels_btn_lvl_pressed(level: int) -> void:
	$Game.load_level(level)
	show_view(GAME)


func _on_Game_btn_back_pressed() -> void:
	show_view(LEVELS)
	$Game.unload_level()
