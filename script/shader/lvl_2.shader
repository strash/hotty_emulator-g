shader_type canvas_item;
render_mode blend_mix;


vec4 circle(vec2 uv, vec2 pos, float radius) {
	vec4 color = vec4(0.862745098, 0.5725490196, 1.0, 1.0);
	color.a = 1.0 - abs(sin(50.0 / length(pos - uv)));
	float d = length(pos - uv) - radius;
	float t = clamp(d, 0.0, 1.0);
	return vec4(color.xyz, color.a - t);
}


void fragment() {
	vec2 circle_pos[] = { vec2(0.25, 0.25), vec2(0.75, 0.5), vec2(0.25, 0.75) };
	
	vec2 uv = FRAGCOORD.xy;
	float radius = 0.25 * (1.0 / SCREEN_PIXEL_SIZE.x);

	vec4 color = vec4(0.0);
	for (int i = 0; i < circle_pos.length(); i++) {
		vec2 position = 1.0 / SCREEN_PIXEL_SIZE.xy * circle_pos[i].xy;
		vec4 new_color = circle(uv, position, radius);
		if (new_color.a > 0.0) {
			color += new_color;
		}
	}
	COLOR = color;
}