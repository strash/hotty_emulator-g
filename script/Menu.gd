extends Control


signal btn_start_pressed
signal btn_levels_pressed


# BUILTINS - - - - - - - - -


#func _ready() -> void:
#	pass


# METHODS - - - - - - - - -


# SIGNALS - - - - - - - - -

func _on_BtnStart_pressed() -> void:
	emit_signal("btn_start_pressed")


func _on_BtnLevel_pressed() -> void:
	emit_signal("btn_levels_pressed")
