extends Control


signal btn_back_pressed


enum { VISIBLE, HIDDEN }


const PROP = [
	{
		btn = {
			top = [8.0, 11.0],
			bottom = [1.0, 6.0]
		},
		img = {
			top = [199.0, 146.0],
			bottom = [206.0, 321.0]
		},
	},
	{
		btn = {
			top = [10.0, 6.0],
			bottom = [-23.0, -6.0]
		},
		img = {
			top = [206.0, 128.0],
			bottom = [139.0, 280.0]
		},
	},
	{
		btn = {
			top = [5.0, 1.0],
			bottom = [-3.0, 8.0]
		},
		img = {
			top = [183.0, 151.0],
			bottom = [150.0, 325.0]
		},
	},
]


var status_top = HIDDEN
var status_bottom = HIDDEN
var status_effect = HIDDEN
var status_interpolation = 0.15


# BUILTINS - - - - - - - - -


#func _ready() -> void:
#	pass


# METHODS - - - - - - - - -


func load_level(level: int) -> void:
	var prop = PROP[level - 1]
	$Bg.texture = load("res://assets/image/bg_lvl_" + str(level) + ".png")

	$BtnTop/Image.set_size(Vector2.ZERO)
	$BtnTop/Image.texture = load("res://assets/image/img_t_lvl_" + str(level) + ".png")
	$BtnTop/Image.set_position(Vector2(prop.btn.top[0], prop.btn.top[1]))
	$BtnBottom/Image.set_size(Vector2.ZERO)
	$BtnBottom/Image.texture = load("res://assets/image/img_b_lvl_" + str(level) + ".png")
	$BtnBottom/Image.set_position(Vector2(prop.btn.bottom[0], prop.btn.bottom[1]))

	$Bg/Top.set_size(Vector2.ZERO)
	$Bg/Top.texture = load("res://assets/image/img_t_lvl_" + str(level) + ".png")
	$Bg/Top.set_position(Vector2(prop.img.top[0], prop.img.top[1]))
	$Bg/Bottom.set_size(Vector2.ZERO)
	$Bg/Bottom.texture = load("res://assets/image/img_b_lvl_" + str(level) + ".png")
	$Bg/Bottom.set_position(Vector2(prop.img.bottom[0], prop.img.bottom[1]))

	_show_hide_top(HIDDEN)
	_show_hide_bottom(HIDDEN)
	_show_hide_effect(HIDDEN)


func unload_level() -> void:
	_show_hide_top(HIDDEN)
	_show_hide_bottom(HIDDEN)
	_show_hide_effect(HIDDEN)


func _show_hide_top(status: int) -> void:
	status_top = status
	match status:
		VISIBLE:
			$Tween.interpolate_property($Bg/Top, "modulate:a", 0.0, 1.0, status_interpolation)
			$Tween.interpolate_property($BtnTop/Image, "modulate:a", 1.0, 0.0, status_interpolation)
		HIDDEN:
			$Tween.interpolate_property($Bg/Top, "modulate:a", 1.0, 0.0, status_interpolation)
			$Tween.interpolate_property($BtnTop/Image, "modulate:a", 0.0, 1.0, status_interpolation)
	if not $Tween.is_active():
		$Tween.start()


func _show_hide_bottom(status: int) -> void:
	status_bottom = status
	match status:
		VISIBLE:
			$Tween.interpolate_property($Bg/Bottom, "modulate:a", 0.0, 1.0, status_interpolation)
			$Tween.interpolate_property($BtnBottom/Image, "modulate:a", 1.0, 0.0, status_interpolation)
		HIDDEN:
			$Tween.interpolate_property($Bg/Bottom, "modulate:a", 1.0, 0.0, status_interpolation)
			$Tween.interpolate_property($BtnBottom/Image, "modulate:a", 0.0, 1.0, status_interpolation)
	if not $Tween.is_active():
		$Tween.start()


func _show_hide_effect(status: int) -> void:
	status_effect = status
	match status:
		VISIBLE:
			$Tween.interpolate_property($BtnEffect/Image, "modulate:a", 1.0, 0.0, status_interpolation)
			$Shader.material.set_shader_param("shader_state", true)
		HIDDEN:
			$Tween.interpolate_property($BtnEffect/Image, "modulate:a", 0.0, 1.0, status_interpolation)
			$Shader.material.set_shader_param("shader_state", false)
	if not $Tween.is_active():
		$Tween.start()


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	emit_signal("btn_back_pressed")


func _on_BtnTop_pressed() -> void:
	match status_top:
		VISIBLE:
			_show_hide_top(HIDDEN)
		HIDDEN:
			_show_hide_top(VISIBLE)


func _on_BtnBottom_pressed() -> void:
	match status_bottom:
		VISIBLE:
			_show_hide_bottom(HIDDEN)
		HIDDEN:
			_show_hide_bottom(VISIBLE)


func _on_BtnEffect_pressed() -> void:
	match status_effect:
		VISIBLE:
			_show_hide_effect(HIDDEN)
		HIDDEN:
			_show_hide_effect(VISIBLE)
