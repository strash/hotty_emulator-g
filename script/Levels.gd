extends Control


signal btn_back_pressed
signal btn_lvl_pressed


# BUILTINS - - - - - - - - -


func _ready() -> void:
	pass


# METHODS - - - - - - - - -


# SIGNALS - - - - - - - - -

func _on_BtnBack_pressed() -> void:
	emit_signal("btn_back_pressed")


func _on_BtnLvl1_pressed() -> void:
	emit_signal("btn_lvl_pressed", 1)


func _on_BtnLvl2_pressed() -> void:
	emit_signal("btn_lvl_pressed", 2)


func _on_BtnLvl3_pressed() -> void:
	emit_signal("btn_lvl_pressed", 3)
